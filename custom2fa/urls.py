# django
from django.conf.urls import url
from django.urls import path, include
from django.contrib import admin
from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import login_required
from django.views.defaults import permission_denied
# apps
from apps.otp import forms, views
from apps.otp.decorators import otp_required

urlpatterns = [
    # admin
    url(
        r'^admin/',
        admin.site.urls
    ),
    # login
    path(
        'login/',
        views.OTPLoginView.as_view(),
        name="login"
    ),
    # logout
    path(
        'logout/',
        views.OTPLogoutView.as_view(),
        name="logout"
    ),
    # front
    path(
        '',
        TemplateView.as_view(
            template_name='pages/front.html'
        ),
        name='front'
    ),
    # profile
    path(
        'profile/',
        otp_required( views.OTPProfile.as_view() ),
        name='profile'
    ),
    # token form
    path('verify/',
        views.TokenFormView.as_view(),
        name='token_form'
    ),
    # private
    path(
        'private/',
        otp_required( TemplateView.as_view(
            template_name='pages/private.html'
        )),
        name="private"
    ),
]
