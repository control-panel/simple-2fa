import functools
from django.http import Http404
from django.utils.functional import SimpleLazyObject

class OTPMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user = getattr(request, 'user', None)
        if user is not None:
            request.user = SimpleLazyObject(functools.partial(self._verify_user, request, user))
        return self.get_response(request)

    def _verify_user(self, request, user):
        """
        Sets OTP-related fields on an authenticated user.
        """

        user.is_verified = False
        if user.is_authenticated:
            user.is_verified = request.session.get('otp_login', False) is True
        return user
