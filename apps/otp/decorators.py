from django.contrib.auth.decorators import user_passes_test
from django.conf import settings

def otp_required(view=None, redirect_field_name='next', login_url=None):

    if login_url is None:
        login_url = settings.LOGIN_URL

    def test(user):
        return user.is_verified or (user.is_authenticated and not user.has_2fa_enabled )

    decorator = user_passes_test(
        test,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )

    return decorator if (view is None) else decorator(view)
