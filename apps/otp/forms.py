
import binascii
from django import forms
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, UserChangeForm
from django.core.mail import send_mail
from .models import OTPUser
from . import utils
import time


class OTPUserCreationForm(UserCreationForm):

    class Meta:
        model = OTPUser
        fields = ('username', 'email',)


class OTPUserChangeForm(UserChangeForm):

    class Meta:
        model = OTPUser
        fields = ('username', 'email',)


class TokenForm(forms.Form):

    token = forms.IntegerField(
        required=False,
        widget=forms.PasswordInput,
        min_value=0,
        max_value=999999,
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user")
        super(TokenForm, self).__init__(*args, **kwargs)

    def clean(self):
        token  = self.cleaned_data['token']
        device = self.user.device
        if not self.user.verify_token(token):
            raise forms.ValidationError(
                "El token es erróneo. %s" % \
                'Te hemos enviado otro token por correo electrónico' if device == '1' \
                else 'Comprueba la aplicación e introduce nuevamente el token'
            )
        return self.cleaned_data


class ProfileForm(forms.ModelForm):

    token = forms.IntegerField(
        required=False,
        widget=forms.PasswordInput,
        min_value=0,
        max_value=999999,
    )

    def clean(self):
        device = self.cleaned_data['device']
        token = self.cleaned_data['token']
        if device != self.instance.device:
            if token:
                if not self.instance.verify_token(token):
                    raise forms.ValidationError(
                        "El token es erróneo. %s" % \
                        'Te hemos enviado otro token por correo electrónico' if device == '1' \
                        else 'Comprueba la aplicación e introduce nuevamente el token'
                    )
                return self.cleaned_data
            elif device == '1':
                self.instance.send_token_email()
                raise forms.ValidationError(
                    "Por favor introduce el token que te hemos enviado "
                    "por correo para completar este paso",
                code='email_sent')
            elif device == '2':
                raise forms.ValidationError(
                    "Por favor escanea el código QR con una app de validación e "
                    "introduce el token para completar este paso",
                code='qr_code')
        return self.cleaned_data

    class Meta:
        model = OTPUser
        fields = ['device', ]
