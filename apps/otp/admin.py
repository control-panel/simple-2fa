# users/admin.py
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from .forms import OTPUserCreationForm, OTPUserChangeForm
from .models import OTPUser

class OTPUserAdmin(UserAdmin):
    model = OTPUser
    add_form = OTPUserCreationForm
    form = OTPUserChangeForm
    list_display = [ 'username', 'email',]
    fieldsets = UserAdmin.fieldsets + (
        ('OTP', {
            'fields': ('device', 'key')
        })
    ,)
admin.site.register(OTPUser, OTPUserAdmin)
