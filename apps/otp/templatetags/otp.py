# django
from django import template
from django.forms.forms import NON_FIELD_ERRORS

register = template.Library()

@register.filter
def has_nonform_error(value, error):
    return value.has_error(NON_FIELD_ERRORS, error)
