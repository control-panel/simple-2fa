# django
from django import views
from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic.edit import UpdateView
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.utils.module_loading import import_string
from django.conf import settings
from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
# contrib
from . import forms
import qrcode
import qrcode.image.svg
# project
from . import models
from . import utils

class OTPLoginView(LoginView):
    """
    Display the login form and handle the login action.
    """
    template_name = 'registration/login.html'
    redirect_authenticated_user = True

    def get_success_url(self):
        urls = {
            '0' : 'profile',
            '1' : 'token_form',
            '2' : 'token_form'
        }
        return reverse( urls[ self.request.user.device ] )

class OTPLogoutView(LogoutView):
    """
    Display the login form and handle the login action.
    """
    template_name = 'registration/login.html'
    redirect_authenticated_user = True

    def get_success_url(self):
        request.session['otp_login'] = False
        return '/'

class OTPProfile(UpdateView):
    model  = models.OTPUser
    form_class = forms.ProfileForm
    template_name = 'registration/form-profile.html'
    success_url = reverse_lazy('profile')

    def get_context_data(self, **kwargs):
        user = self.request.user
        context = super(OTPProfile, self).get_context_data(**kwargs)
        try:
            username = user.get_username()
        except AttributeError:
            username = user.username
        issuer = getattr(settings, 'OTP-ISSUER', 'maadix')
        context['url'] = utils.get_otpauth_url(
            accountname=username,
            issuer=issuer,
            secret=user.b32_key,
        )
        return context

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        messages.success(
            self.request,
            'Has cambiado el modo de autenticación con éxito'
        )
        if self.request.user.device is '0':
            self.request.session['otp_login'] = False
        self.request.session['otp_login'] = True
        return super(OTPProfile, self).get_success_url()


class TokenFormView(LoginRequiredMixin, FormView):

    template_name = 'registration/form-token.html'
    form_class = forms.TokenForm
    success_url = reverse_lazy('profile')

    def dispatch(self, request, *args, **kwargs):
        if request.user.device == '1':
            request.user.send_token_email()
        return super(TokenFormView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(TokenFormView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        self.request.session['otp_login'] = True
        return super(TokenFormView, self).form_valid(form)
