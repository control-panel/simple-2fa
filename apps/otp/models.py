# python
import os
import binascii
import time
from base64 import b32encode
from binascii import unhexlify
# django
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.encoding import force_str
from django.core.mail import send_mail
# project
from .utils import totp

def random_hex(length=20):
    """
    Returns a string of random bytes encoded as hex.

    This uses :func:`os.urandom`, so it should be suitable for generating
    cryptographic keys.

    :param int length: The number of (decoded) bytes to return.

    :returns: A string of hex digits.
    :rtype: str

    """
    return os.urandom(length).hex()

def random_hex_str(length=20):
    # Could be removed once we depend on django_otp > 0.7.5
    return force_str(random_hex(length=length))

class OTPUser(AbstractUser):

    device = models.CharField(
        'Device',
        max_length=1,
        choices=(
            ('0', 'No usar 2FA'),
            ('1', 'Correo electrónico'),
            ('2', 'App de autenticación')
        ),
        default='0'

    )
    key = models.CharField(
        max_length=40,
        default=random_hex_str,
        help_text="Hex-encoded secret key"
    )
    last_token_time = models.BigIntegerField(
        default = -1,
    )

    def verify_token(self, other):
        key = binascii.unhexlify(self.key.encode())
        token = totp(key)
        t = time.time()
        if t > self.last_token_time and token == other:
            self.last_token_time = t
            self.save(update_fields=['last_token_time'])
            return True
        return False

    @property
    def b32_key(self):
        rawkey = unhexlify(self.key.encode('ascii'))
        return b32encode(rawkey).decode('utf-8')

    @property
    def has_2fa_enabled(self):
        return self.device != '0'

    def send_token_email(self):
        key   = binascii.unhexlify(self.key.encode())
        token = totp(key)
        print(token)
        send_mail(
            subject="Your OTP Password",
            message="Your OTP password is %s" % token,
            from_email='no-reply@maadix.net',
            recipient_list=[self.email]
        )

    def __str__(self):
        return self.username
